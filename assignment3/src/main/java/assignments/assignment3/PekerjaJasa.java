package assignments.assignment3;

public class PekerjaJasa extends Manusia {

    // constructor
    public PekerjaJasa(String nama) {
        super(nama);
    }

    public String toString() {
        return String.format("%s %s", "PEKERJA JASA", getNama());
    }
}