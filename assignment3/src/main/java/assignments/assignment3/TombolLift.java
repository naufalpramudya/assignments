package assignments.assignment3;

public class TombolLift extends Benda {

    // constructor
    public TombolLift(String nama) {
        super(nama);
    }

    @Override // method untuk menambahkan persentase menular benda
    public void tambahPersentase() {
        this.setPersentaseMenular(this.getPersentaseMenular() + 20);
    }

    public String toString() {
        return String.format("%s %s", "TOMBOL LIFT", getNama());
    }
}