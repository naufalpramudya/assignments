package assignments.assignment3;

public class Positif implements Status {

    public String getStatus() {
        return "Positif";
    }

    // method untuk menularkan covid ke carrier lain
    public void tularkan(Carrier penular, Carrier tertular) {

        // jika terjadi interaksi antar benda, maka tidak terjadi apa-apa
        if (penular.getTipe().equals("Benda") && tertular.getTipe().equals("Benda")) {

        } else if (penular.getTipe().equals("Manusia") && tertular.getTipe().equals("Benda")) {
            if (((Benda) tertular).getPersentaseMenular() >= 100) {
                tertular.ubahStatus("Positif");
                try {
                    // untuk menambahkan carrier di rantai penular, ke rantai tertular
                    for (int i = 0; i < penular.getRantaiPenular().size(); i++) {
                        tertular.addRantaiPenular(penular.getRantaiPenular().get(i));
                    }
                    // untuk menambahkan jumlah total kasus dan aktif kasus carrier yang ada di
                    // dalam rantai
                    for (int i = 0; i < penular.getRantaiPenular().size(); i++) {
                        penular.getRantaiPenular().get(i).tambahTotalKasusDisebabkan();
                        penular.getRantaiPenular().get(i).tambahAktifKasusDisebabkan();
                    }
                    tertular.addRantaiPenular(penular);

                } catch (BelumTertularException ex) {
                    System.out.println(ex.getMessage());
                }

            } else {
                ((Benda) tertular).tambahPersentase();
                // jika persentase menular suatu benda >= 100, maka akan menjadi positif covid
                if (((Benda) tertular).getPersentaseMenular() >= 100) {
                    tertular.ubahStatus("Positif");
                    try {
                        // untuk menambahkan carrier di rantai penular, ke rantai tertular
                        for (int i = 0; i < penular.getRantaiPenular().size(); i++) {
                            tertular.addRantaiPenular(penular.getRantaiPenular().get(i));
                        }
                        // untuk menambahkan jumlah total kasus dan aktif kasus carrier yang ada di
                        // dalam rantai
                        for (int i = 0; i < penular.getRantaiPenular().size(); i++) {
                            penular.getRantaiPenular().get(i).tambahTotalKasusDisebabkan();
                            penular.getRantaiPenular().get(i).tambahAktifKasusDisebabkan();
                        }
                        tertular.addRantaiPenular(penular);

                    } catch (BelumTertularException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
        } else {
            tertular.ubahStatus("Positif");
            try {
                // untuk menambahkan carrier di rantai penular, ke rantai tertular
                for (int i = 0; i < penular.getRantaiPenular().size(); i++) {
                    tertular.addRantaiPenular(penular.getRantaiPenular().get(i));
                }
                // untuk menambahkan jumlah total kasus dan aktif kasus carrier yang ada di
                // dalam rantai
                for (int i = 0; i < penular.getRantaiPenular().size(); i++) {
                    penular.getRantaiPenular().get(i).tambahTotalKasusDisebabkan();
                    penular.getRantaiPenular().get(i).tambahAktifKasusDisebabkan();
                }
                tertular.addRantaiPenular(penular);
            } catch (BelumTertularException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

}