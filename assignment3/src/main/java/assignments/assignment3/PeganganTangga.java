package assignments.assignment3;

public class PeganganTangga extends Benda {

    // constructor
    public PeganganTangga(String name) {
        super(name);
    }

    @Override // method untuk menambahkan persentase menular benda
    public void tambahPersentase() {
        this.setPersentaseMenular(this.getPersentaseMenular() + 20);
    }

    public String toString() {
        return String.format("%s %s", "PEGANGAN TANGGA", getNama());
    }
}