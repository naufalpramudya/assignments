package assignments.assignment3;

public class Ojol extends Manusia {

    // constructor
    public Ojol(String nama) {
        super(nama);
    }

    public String toString() {
        return String.format("%s %s", "OJOL", getNama());
    }
}