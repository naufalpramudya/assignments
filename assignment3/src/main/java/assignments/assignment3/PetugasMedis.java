package assignments.assignment3;

public class PetugasMedis extends Manusia {

    private int jumlahDisembuhkan;

    // constructor
    public PetugasMedis(String nama) {
        super(nama);
    }

    // method khusus milik petugas medis untuk menyembuhkan manusia yang positif
    // sehingga menjadi negatif
    public void obati(Manusia manusia) {
        if (manusia.getStatusCovid().equals("Positif")) {
            try {
                // untuk mengurangi aktif kasus carrier di rantai penular manusia
                for (int i = 0; i < manusia.getRantaiPenular().size(); i++) {
                    manusia.getRantaiPenular().get(i).kurangiAktifKasusDisebabkan();
                }
                manusia.clearRantaiPenular();
            } catch (BelumTertularException ex) {
                System.out.println(ex.getMessage());
            }
            manusia.ubahStatus("Negatif");
        }
        this.tambahJumlahDisembuhkan();
        manusia.tambahSembuh();
    }

    // getter jumlah disembuhkan petugas medis
    public int getJumlahDisembuhkan() {
        return this.jumlahDisembuhkan;
    }

    // method untuk menambahkan jumlah disembuhkan petugas medis
    public void tambahJumlahDisembuhkan() {
        this.jumlahDisembuhkan += 1;
    }

    public String toString() {
        return String.format("%s %s", "PETUGAS MEDIS", getNama());
    }
}