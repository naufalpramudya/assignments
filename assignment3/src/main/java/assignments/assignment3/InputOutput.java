package assignments.assignment3;

import java.io.*;
import java.util.List;

public class InputOutput {

    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile;
    private World world;

    // constructor
    public InputOutput(String inputType, String inputFile, String outputType, String outputFile) {

        this.inputFile = inputFile;
        this.outputFile = outputFile;
        setBufferedReader(inputType);
        setPrintWriter(outputType);
    }

    // setter buffered reader untuk input
    public void setBufferedReader(String inputType) {

        try {
            if (inputType.equalsIgnoreCase("text")) {
                FileReader fr = new FileReader(this.inputFile);
                br = new BufferedReader(fr);
            } else if (inputType.equalsIgnoreCase("terminal")) {
                br = new BufferedReader(new InputStreamReader(System.in));
            }
        } catch (IOException e) {
            System.out.println("File tidak ditemukan");
        }
    }

    // setter print writer untuk output
    public void setPrintWriter(String outputType) {

        try {
            if (outputType.equalsIgnoreCase("text")) {
                File file = new File(outputFile);
                pw = new PrintWriter(file);
            } else if (outputType.equalsIgnoreCase("terminal")) {
                pw = new PrintWriter(System.out);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // method untuk mengoperasikan input dari user dan mengembalikan output
    public void run() throws IOException, BelumTertularException {
        world = new World();
        boolean hasChosenExit = false;

        while (!hasChosenExit) {
            String line = br.readLine();
            if (line != null) {
                String[] in = line.strip().split(" ");
                String command = in[0];
                if (command.equalsIgnoreCase("add")) {
                    String tipe = in[1];
                    String nama = in[2];
                    world.createObject(tipe, nama);

                } else if (command.equalsIgnoreCase("interaksi")) {
                    String obj1 = in[1];
                    String obj2 = in[2];
                    Carrier cr1 = world.getCarrier(obj1);
                    Carrier cr2 = world.getCarrier(obj2);
                    if (cr1 != null && cr2 != null) {
                        cr1.interaksi(cr2);
                    }

                } else if (command.equalsIgnoreCase("positifkan")) {
                    String obj = in[1];
                    Carrier cr = world.getCarrier(obj);
                    if (cr != null) {
                        cr.ubahStatus("Positif");
                    }

                } else if (command.equalsIgnoreCase("sembuhkan")) {
                    String obj1 = in[1];
                    String obj2 = in[2];
                    PetugasMedis petugasMedis = (PetugasMedis) world.getCarrier(obj1);
                    Manusia pasien = (Manusia) world.getCarrier(obj2);
                    if (petugasMedis != null && pasien != null) {
                        petugasMedis.obati(pasien);
                    }

                } else if (command.equalsIgnoreCase("bersihkan")) {
                    String obj1 = in[1];
                    String obj2 = in[2];
                    CleaningService cleaningService = (CleaningService) world.getCarrier(obj1);
                    Benda benda = (Benda) world.getCarrier(obj2);
                    if (cleaningService != null && benda != null) {
                        cleaningService.bersihkan(benda);
                    }

                } else if (command.equalsIgnoreCase("rantai")) {
                    String obj = in[1];
                    Carrier cr = world.getCarrier(obj);
                    if (cr != null) {
                        try {
                            List<Carrier> rantai = cr.getRantaiPenular();
                            String keterangan = "";
                            if (rantai.size() == 0) {
                                pw.println(String.format("Rantai penyebaran %s: %s", cr.toString(), cr.toString()));
                            } else {
                                for (int i = 0; i < rantai.size(); i++) {
                                    keterangan += String.format("%s -> ", rantai.get(i).toString());
                                }
                                pw.println(String.format("Rantai penyebaran %s: %s%s", cr.toString(), keterangan,
                                        cr.toString()));
                            }
                        } catch (BelumTertularException ex) {
                            pw.println(ex.toString());
                        }
                    }

                } else if (command.equalsIgnoreCase("total_kasus_dari_objek")) {
                    String obj = in[1];
                    Carrier cr = world.getCarrier(obj);
                    if (cr != null) {
                        int totalKasus = cr.getTotalKasusDisebabkan();
                        pw.println(String.format("%s telah menyebarkan virus COVID ke %d objek", cr.toString(),
                                totalKasus));
                    }

                } else if (command.equalsIgnoreCase("aktif_kasus_dari_objek")) {
                    String obj = in[1];
                    Carrier cr = world.getCarrier(obj);
                    if (cr != null) {
                        int aktifKasus = cr.getAktifKasusDisebabkan();
                        pw.println(String.format(
                                "%s telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak %d objek",
                                cr.toString(), aktifKasus));
                    }

                } else if (command.equalsIgnoreCase("total_sembuh_manusia")) {
                    pw.println(
                            String.format("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: %d kasus",
                                    Manusia.getJumlahSembuh()));

                } else if (command.equalsIgnoreCase("total_sembuh_petugas_medis")) {
                    String obj = in[1];
                    PetugasMedis medis = (PetugasMedis) world.getCarrier(obj);
                    if (medis != null) {
                        pw.println(String.format("%s menyembuhkan %d manusia", medis.toString(),
                                medis.getJumlahDisembuhkan()));
                    }

                } else if (command.equalsIgnoreCase("total_bersih_cleaning_service")) {
                    String obj = in[1];
                    CleaningService cl = (CleaningService) world.getCarrier(obj);
                    if (cl != null) {
                        pw.println(String.format("%s membersihkan %d benda", cl.toString(), cl.getJumlahDibersihkan()));
                    }

                } else if (command.equalsIgnoreCase("exit")) {
                    hasChosenExit = true;
                }
            }
        }
        pw.close();
    }
}