package assignments.assignment3;

//interface status covid
public interface Status {
    public String getStatus();

    public void tularkan(Carrier penular, Carrier tertular);
}