package assignments.assignment3;

public class Jurnalis extends Manusia {

    // constructor
    public Jurnalis(String nama) {
        super(nama);
    }

    public String toString() {
        return String.format("%s %s", "JURNALIS", getNama());
    }
}