package assignments.assignment3;

public class Pintu extends Benda {

    // constructor
    public Pintu(String nama) {
        super(nama);
    }

    @Override // method untuk menambahkan persentase menular benda
    public void tambahPersentase() {
        this.setPersentaseMenular(this.getPersentaseMenular() + 30);
    }

    public String toString() {
        return String.format("%s %s", "PINTU", getNama());
    }
}