package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier {

    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    // constructor
    public Carrier(String nama, String tipe) {
        this.nama = nama;
        this.tipe = tipe;
        this.statusCovid = new Negatif();
        this.rantaiPenular = new ArrayList<>();
    }

    // getter nama
    public String getNama() {
        return this.nama;
    }

    // getter tipe
    public String getTipe() {
        return this.tipe;
    }

    // getter status covid
    public String getStatusCovid() {
        return this.statusCovid.getStatus();
    }

    // getter kasus aktif yang disebabkan carrier
    public int getAktifKasusDisebabkan() {
        return aktifKasusDisebabkan;
    }

    // method untuk menambahkan jumlah kasus aktif
    public void tambahAktifKasusDisebabkan() {
        aktifKasusDisebabkan += 1;
    }

    // method untuk mengurangi jumlah kasus aktif
    public void kurangiAktifKasusDisebabkan() {
        aktifKasusDisebabkan -= 1;
    }

    // getter total kasus yang disebabkan carrier
    public int getTotalKasusDisebabkan() {
        return totalKasusDisebabkan;
    }

    // method untuk menambahkan jumlah total kasus
    public void tambahTotalKasusDisebabkan() {
        totalKasusDisebabkan += 1;
    }

    // method untuk mengurangi jumlah total kasus
    public void kurangiTotalKasusDisebabkan() {
        totalKasusDisebabkan -= 1;
    }

    // getter rantai penular carrier
    public List<Carrier> getRantaiPenular() throws BelumTertularException {
        // jika status covid carrier negatif, maka akan muncul BelumTertularException
        if (this.getStatusCovid().equals("Negatif")) {
            throw new BelumTertularException(String.format("%s berstatus negatif", this.toString()));
        } else {
            return this.rantaiPenular;
        }
    }

    // method untuk menambahkan carrier ke rantai penular
    public void addRantaiPenular(Carrier penular) {
        this.rantaiPenular.add(penular);
    }

    // method untuk mengosongkan rantai penular
    public void clearRantaiPenular() {
        this.rantaiPenular.clear();
    }

    // method untuk mengubah status covid suatu carrier
    public void ubahStatus(String status) {
        if (status.equals("Negatif")) {
            this.statusCovid = new Negatif();
        } else if (status.equals("Positif")) {
            this.statusCovid = new Positif();
        }
    }

    // method untuk dua carrier yang berinteraksi ketika mendapat query INTERAKSI
    public void interaksi(Carrier lain) {
        if (this.getStatusCovid().equals("Positif") && lain.getStatusCovid().equals("Negatif")) {
            this.statusCovid.tularkan(this, lain);
            if (lain.getStatusCovid().equals("Positif")) {
                this.tambahTotalKasusDisebabkan();
                this.tambahAktifKasusDisebabkan();
            }
            try {
                // untuk menghindari penambahan kasus 2x disaat terdapat dua carrier yang sama
                // dalam suatu rantai
                for (int i = 0; i < this.getRantaiPenular().size(); i++) {
                    if (this.getRantaiPenular().get(i).equals(lain)) {
                        lain.kurangiTotalKasusDisebabkan();
                        lain.kurangiAktifKasusDisebabkan();
                    }
                }
            } catch (BelumTertularException ex) {
                System.out.println(ex.getMessage());
            }

        } else if (lain.getStatusCovid().equals("Positif") && this.getStatusCovid().equals("Negatif")) {
            lain.statusCovid.tularkan(lain, this);
            if (this.getStatusCovid().equals("Positif")) {
                lain.tambahAktifKasusDisebabkan();
                lain.tambahTotalKasusDisebabkan();
            }
            try {
                // untuk menghindari penambahan kasus 2x disaat terdapat dua carrier yang sama
                // dalam suatu rantai
                for (int i = 0; i < lain.getRantaiPenular().size(); i++) {
                    if (lain.getRantaiPenular().get(i).equals(this)) {
                        this.kurangiTotalKasusDisebabkan();
                        this.kurangiAktifKasusDisebabkan();
                    }
                }
            } catch (BelumTertularException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public abstract String toString();

}
