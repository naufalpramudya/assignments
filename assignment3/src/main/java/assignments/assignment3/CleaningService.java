package assignments.assignment3;

public class CleaningService extends Manusia {

    private int jumlahDibersihkan;

    // constructor
    public CleaningService(String nama) {
        super(nama);
    }

    // method khusus milik cleaning service untuk membersihkan benda yang positif
    // sehingga menjadi negatif
    public void bersihkan(Benda benda) {
        if (benda.getPersentaseMenular() >= 100 && benda.getStatusCovid().equals("Positif")) {
            try {
                // untuk mengurangi aktif kasus yang disebabkan oleh rantai penular ke benda
                for (int i = 0; i < benda.getRantaiPenular().size(); i++) {
                    benda.getRantaiPenular().get(i).kurangiAktifKasusDisebabkan();
                }
                benda.clearRantaiPenular();
            } catch (BelumTertularException ex) {
                System.out.println(ex.getMessage());
            }
            benda.setPersentaseMenular(0);
            benda.ubahStatus("Negatif");
        } else {
            benda.setPersentaseMenular(0);
        }
        this.tambahJumlahDibersihkan();
    }

    // getter jumlah dibersihkan cleaning service
    public int getJumlahDibersihkan() {
        return this.jumlahDibersihkan;
    }

    // method untuk menambahkan jumlah dibersihkan cleaning service
    public void tambahJumlahDibersihkan() {
        this.jumlahDibersihkan += 1;
    }

    public String toString() {
        return String.format("%s %s", "CLEANING SERVICE", getNama());
    }
}