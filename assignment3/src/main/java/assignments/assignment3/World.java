package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World {

    public List<Carrier> listCarrier;

    // constructor
    public World() {
        listCarrier = new ArrayList<>();
    }

    // method untuk membuat objek ketika mendapat query ADD
    public Carrier createObject(String tipe, String nama) {
        if (tipe.equals("OJOL")) {
            Carrier ojol = new Ojol(nama);
            listCarrier.add(ojol);
        } else if (tipe.equals("JURNALIS")) {
            Carrier jurnalis = new Jurnalis(nama);
            listCarrier.add(jurnalis);
        } else if (tipe.equals("PEKERJA_JASA")) {
            Carrier pj = new PekerjaJasa(nama);
            listCarrier.add(pj);
        } else if (tipe.equals("CLEANING_SERVICE")) {
            Carrier cs = new CleaningService(nama);
            listCarrier.add(cs);
        } else if (tipe.equals("PETUGAS_MEDIS")) {
            Carrier pm = new PetugasMedis(nama);
            listCarrier.add(pm);
        } else if (tipe.equals("PEGANGAN_TANGGA")) {
            Carrier pg = new PeganganTangga(nama);
            listCarrier.add(pg);
        } else if (tipe.equals("PINTU")) {
            Carrier pintu = new Pintu(nama);
            listCarrier.add(pintu);
        } else if (tipe.equals("TOMBOL_LIFT")) {
            Carrier tl = new TombolLift(nama);
            listCarrier.add(tl);
        } else if (tipe.equals("ANGKUTAN_UMUM")) {
            Carrier ang = new AngkutanUmum(nama);
            listCarrier.add(ang);
        }

        return null;
    }

    // method yang akan mengembalikan carrier berdasarkan nama
    public Carrier getCarrier(String nama) {
        for (int i = 0; i < listCarrier.size(); i++) {
            if (listCarrier.get(i).getNama().equals(nama)) {
                return listCarrier.get(i);
            }
        }
        return null;
    }
}
