package assignments.assignment3;

public class AngkutanUmum extends Benda {

    // constructor
    public AngkutanUmum(String nama) {
        super(nama);
    }

    @Override // method untuk menambahkan persentase menular
    public void tambahPersentase() {
        this.setPersentaseMenular(this.getPersentaseMenular() + 35);
    }

    public String toString() {
        return String.format("%s %s", "ANGKUTAN UMUM", getNama());
    }
}