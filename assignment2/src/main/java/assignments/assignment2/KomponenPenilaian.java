package assignments.assignment2;

import java.util.ArrayList;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        this.nama = nama;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
        this.bobot = bobot;
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian
     * templat.
     * 
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * 
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        this.butirPenilaian[idx] = butir;
    }

    public String getNama() {
        return this.nama;
    }

    public double getRerata() {
        if (this.butirPenilaian.length == 0) {
            return 0.00;
        } else {
            double totalNilaiButir = 0;
            int notNull = 0;
            for (ButirPenilaian x : this.butirPenilaian) {
                if (x == null) {
                    continue;
                } else {
                    totalNilaiButir += x.getNilai();
                    notNull += 1;
                }
            }
            if (notNull == 0) {
                return 0.00;
            } else {
                return totalNilaiButir / notNull;
            }
        }
    }

    public double getNilai() {
        return this.getRerata() * this.bobot / 100;
    }

    public String getDetail() {
        String butirButir = "";
        ArrayList<Integer> notNull = new ArrayList<>();
        ArrayList<ButirPenilaian> butirNotNull = new ArrayList<>();
        for (int i = 0; i < this.butirPenilaian.length; i++) {
            if (this.butirPenilaian[i] == null) {
                continue;
            } else {
                notNull.add(i);
                butirNotNull.add(this.butirPenilaian[i]);
            }
        }
        if (butirNotNull.size() == 1) {
            butirButir = this.nama + ": " + butirNotNull.get(0) + "\n";
            return "~~~ " + this.nama + " (" + this.bobot + "%)" + " ~~~\n" + butirButir + "Kontribusi Nilai Akhir: "
                    + String.valueOf(String.format("%.2f", this.getNilai())) + "\n";
        } else {
            for (int i = 0; i < notNull.size(); i++) {
                butirButir += this.nama + Integer.toString(notNull.get(i) + 1) + ": " + butirNotNull.get(i).toString()
                        + "\n";
            }
        }
        return "~~~ " + this.nama + " (" + this.bobot + "%)" + " ~~~\n" + butirButir + "Rerata: "
                + String.valueOf(String.format("%.2f", this.getRerata())) + "\nKontribusi Nilai Akhir: "
                + String.valueOf(String.format("%.2f", this.getNilai())) + "\n";
    }

    @Override
    public String toString() {
        String rerata = String.format("%.2f", this.getRerata());
        return "Rerata " + this.nama + ": " + rerata;

    }

}
