package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    // public ButirPenilaian() {
    // this(0, false);
    // }

    public ButirPenilaian(double nilai, boolean terlambat) {
        this.nilai = nilai;
        this.terlambat = terlambat;
    }

    public double getNilai() {
        if (this.nilai < 0) {
            return 0.00;
        } else {
            if (this.terlambat) {
                return this.nilai - this.nilai * (double) PENALTI_KETERLAMBATAN / 100;
            } else {
                return this.nilai;
            }
        }

    }

    @Override
    public String toString() {
        String s = String.format("%.2f", this.getNilai());
        if (this.terlambat == true) {
            return s + " " + "(T)";
        } else {
            return s;
        }
    }
}
