package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        for (int i = 0; i < this.komponenPenilaian.length; i++) {
            if (namaKomponen.equals(this.komponenPenilaian[i].getNama())) {
                return this.komponenPenilaian[i];
            } else {
                continue;
            }
        }
        return null;
    }

    public String getNpm() {
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * 
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A"
                : nilai >= 80 ? "A-"
                        : nilai >= 75 ? "B+"
                                : nilai >= 70 ? "B"
                                        : nilai >= 65 ? "B-"
                                                : nilai >= 60 ? "C+" : nilai >= 55 ? "C" : nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * 
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
        String rekap = "";
        double totalNilai = 0.00;
        double rerataNilai = 0.00;
        int jml = 0;
        for (int i = 0; i < this.komponenPenilaian.length; i++) {
            rekap += this.komponenPenilaian[i] + "\n";
            totalNilai += this.komponenPenilaian[i].getRerata();
            jml++;
        }

        rerataNilai = totalNilai / jml;
        String rata2Nilai = String.format("%.2f", rerataNilai);
        return rekap + "Nilai Akhir: " + rata2Nilai + "\n" + "Huruf: " + Mahasiswa.getHuruf(rerataNilai) + "\n"
                + Mahasiswa.getKelulusan(rerataNilai);
    }

    public String toString() {
        return this.npm + " - " + this.nama;
    }

    public String getDetail() {
        String detail = "";
        double nilaiAkhir = 0.00;
        for (int i = 0; i < this.komponenPenilaian.length; i++) {
            detail += this.komponenPenilaian[i].getDetail() + "\n";
            nilaiAkhir += this.komponenPenilaian[i].getNilai();
        }
        return detail + "Nilai Akhir: " + String.format("%.2f", nilaiAkhir) + "\n" + "Huruf: "
                + Mahasiswa.getHuruf(nilaiAkhir) + "\n" + Mahasiswa.getKelulusan(nilaiAkhir);
    }

    @Override
    public int compareTo(Mahasiswa other) {
        return this.npm.compareTo(other.npm);
    }
}
