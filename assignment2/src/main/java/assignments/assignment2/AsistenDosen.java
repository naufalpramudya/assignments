package assignments.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AsistenDosen {
    private List<Mahasiswa> daftarMahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        return this.kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        daftarMahasiswa.add(mahasiswa);
        Collections.sort(daftarMahasiswa);
    }

    public Mahasiswa getMahasiswa(String npm) {
        for (int i = 0; i < daftarMahasiswa.size(); i++) {
            if (!daftarMahasiswa.get(i).equals(null)) {
                if (daftarMahasiswa.get(i).getNpm().equals(npm)) {
                    return daftarMahasiswa.get(i);
                } else {
                    continue;
                }
            }
        }
        return null;
    }

    public String rekap() {
        String rekapMahasiswa = "";
        for (int i = 0; i < daftarMahasiswa.size(); i++) {
            rekapMahasiswa += daftarMahasiswa.get(i).toString() + "\n" + daftarMahasiswa.get(i).rekap() + "\n" + "\n";
        }
        return rekapMahasiswa;
    }

    public String toString() {
        return this.kode + " - " + this.nama;
    }
}
