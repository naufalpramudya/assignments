package assignments.assignment1;

import java.util.ArrayList;
import java.util.Scanner;

public class HammingCode {
    // Inisialisasi macam-macam perintah untuk program
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    /**
     * Method ini berfungi untuk mengubah data menjadi Hamming Code. dengan
     * even-parity.
     * 
     */
    public static String encode(String data) {
        ArrayList<String> arrList = new ArrayList<String>();
        int dataLen = data.length();
        int parBit = 1;

        // Menentukan jumlah parity bit
        while (Math.pow(2, parBit) < (dataLen + parBit + 1)) {
            parBit++;
        }
        // Memasukkan data ke arraylist
        for (int i = 0; i < dataLen; i++) {
            String ehe = Character.toString(data.charAt(i));
            arrList.add(ehe);
        }
        // Memasukkan parity bit ke arraylist
        for (int i = 0; i < parBit; i++) {
            int index = (int) Math.pow(2, i) - 1;
            arrList.add(index, "0");
        }
        // Mengoreksi parity bit di dalam arraylist
        for (int i = 0; i < parBit; i++) {
            int counter = 0;
            for (int j = (int) Math.pow(2, i) - 1; j < arrList.size(); j += Math.pow(2, i)) {
                for (int k = 0; k < Math.pow(2, i) && j < arrList.size(); k++) {
                    counter += Integer.parseInt(arrList.get(j));
                    j++;
                }
            } // Jika jumlah bit 1 berjumlah ganjil, maka akan digenapkan
            if (counter % 2 != 0) {
                arrList.set((int) Math.pow(2, i) - 1, "1");
            } else {
                arrList.set((int) Math.pow(2, i) - 1, "0");
            }
        }
        // Mengubah arraylist agar dapat mencetak string
        StringBuffer sb = new StringBuffer();
        for (String x : arrList) {
            sb.append(x);
        }
        String hasilCode = sb.toString();
        return hasilCode;
    }

    /**
     * Method ini berfungsi untuk mengambil data dari Hamming Code. Jika terdapat
     * error, maka akan dikoreksi terlebih dahulu.
     * 
     */
    public static String decode(String code) {
        ArrayList<String> arrList = new ArrayList<String>();
        int codeLen = code.length();
        int parBit = 1;

        // Menentukan jumlah parity bit
        while (Math.pow(2, parBit) < codeLen) {
            parBit++;
        }
        // Memasukkan code ke dalam arraylist
        for (int i = 0; i < code.length(); i++) {
            String ehe = Character.toString(code.charAt(i));
            arrList.add(ehe);
        }
        // Mendeteksi apakah ada error atau tidak
        int error = 0;
        for (int i = 0; i < parBit; i++) {
            int counter = 0;
            for (int j = (int) Math.pow(2, i) - 1; j < arrList.size(); j += Math.pow(2, i)) {
                for (int k = 0; k < Math.pow(2, i) && j < arrList.size(); k++) {
                    counter += Integer.parseInt(arrList.get(j));
                    j++;
                }
            } // Jika jumlah bit 1 ganjil, maka akan menanda grup parity mana saja yang error
            if (counter % 2 != 0) {
                error += (int) Math.pow(2, i);
            } else {
                continue;
            }
        }
        // Mengoreksi error di dalam code
        if (error > 0) {
            if (arrList.get(error - 1).equals("1")) {
                arrList.set(error - 1, "0");
            } else {
                arrList.set(error - 1, "1");
            }
        }
        // Mengeluarkan bit parity dari arraylist
        for (int i = 0; i < parBit; i++) {
            arrList.remove((int) Math.pow(2, i) - 1 - i);
        }
        // Mengubah arraylist menjadi sebuah string
        StringBuffer sb = new StringBuffer();
        for (String x : arrList) {
            sb.append(x);
        }
        String hasilCode = sb.toString();
        return hasilCode;
    }

    /**
     * Main program for Hamming Code.
     * 
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
